from mycroft.skills.core import MycroftSkill, intent_handler
from mycroft.util.log import LOG
from adapt.intent import IntentBuilder
from distro import linux_distribution
from subprocess import Popen

__author__ = "xenrox"


class SystemMaintenanceSkill(MycroftSkill):

    def __init__(self):
        super(SystemMaintenanceSkill, self).__init__(
            name="SystemMaintenanceSkill")

    @intent_handler(IntentBuilder("").require("Update").require("System"))
    def handle_update_intent(self, message):
        update_command = "xterm -e \""
        os_name = linux_distribution(full_distribution_name=False)[0]
        if os_name == "arch":
            update_command += "sudo pacman -Syu --noconfirm"
            self.speak_dialog("update.system")
        elif (os_name == "centos"):
            update_command += "sudo yum -y update"
        elif(os_name == "debian"):
            update_command += "sudo apt-get update && sudo apt-get -y upgrade"
        else:
            self.speak_dialog("cannot.recognize.os")
            return
        update_command += "; echo Press Enter;read\""
        Popen([update_command], shell=True)

    @intent_handler(IntentBuilder("").require("Reboot").require("System"))
    def handle_reboot_intent(self, message):
        self.speak_dialog("reboot.system")
        Popen(["sleep 10 && systemctl reboot"], shell=True)

    @intent_handler(IntentBuilder("").require("Shutdown").require("System"))
    def handle_shutdown_intent(self, message):
        self.speak_dialog("shutdown.system")
        Popen(["sleep 10 && systemctl poweroff"], shell=True)


def create_skill():
    return SystemMaintenanceSkill()
