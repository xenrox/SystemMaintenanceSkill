## System Maintenance Skill
This skill allows you to maintain your GNU/Linux.

## Description
This skill needs xterm to execute the update. The following OS releases are supported:
Arch Linux
Debian
CentOS

## Examples
* "Update my system"

## Credits
xenrox
